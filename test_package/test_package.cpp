#include "wx/wx.h"


#include <wx/app.h>
#include <wx/cmdline.h>

int main()
{
	wxApp::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE, "program");
	wxPrintf("Welcome to the wxWidgets 'console' app!\n");
    return EXIT_SUCCESS;
}
