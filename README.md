# wxbase

Change the version number in conanfile.py (line 85)

Build with :

    conan create . terranum-conan+wxbase/stable --build=missing

Upload with :

    conan upload wxbase/3.2.1@terranum-conan+wxbase/stable --remote=gitlab --all

## Build locally
    
	conan source . --source-folder=_bin
    conan install . --install-folder=_bin
	conan build . --source-folder=_bin --build-folder=_bin
	conan package . --source-folder=_bin --build-folder=_bin --package-folder=_bin/package
	conan export-pkg . terranum-conan+wxbase/stable --source-folder=_bin --build-folder=_bin
	conan test test_package wxbase/3.2.1@terranum-conan+wxbase/stable

